#!/bin/bash




for yoff in 0 5 6 7 8 9 10 11 12
do

    for B0 in 0 100
    do

#	for rseed_seed in 10
	for rseed_seed in {20..29}
	do

	    rseed=$rseed_seed$yoff$B0
	    echo $yoff $B0 $rseed
	    ./NbodyNucleus.exe $yoff $B0 $rseed

	done

    done

done
