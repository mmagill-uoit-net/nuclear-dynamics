
#include "SymplecticEuler.h"


// Slightly incorrect Implict Symplectic Euler solver
// Incorrect insofar as the parallel portion has a race condition
// This isn't a problem here, because every core will
//  either see q_i or q_{i+1}. The latter is slightly better,
//  but not required.

void SymplecticEuler(float* posx, float* posy, float* posz,
		     float* fakeposx, float* fakeposy, float* fakeposz,
		     float* momx, float* momy, float* momz,
		     float* Fx, float* Fy, float* Fz,
		     float* Fpx, float* Fpy, float* Fpz,
		     int* Zlist, int* Slist,
		     float* Ay, float* Az, float B0,
		     float CMx, float CMy, float CMz,
		     int nmin, int nmax, int A, int Z, int visu, float k, float mu,
		     float dt, int max_iter, float tol,
		     FILE* fpos, FILE* fmom, FILE* fmetrics) {

  // Declarations
  int j; // For OpenMP
  float dr, dx, dy, dz; float dp, dpx, dpy, dpz;
  float F, Fp;
  float Ei, Etot;
  float xsum, ysum, zsum; float ravg, r2sum, r2avg;
  float pxsum, pysum, pzsum; float pavg, p2sum, p2avg;
  int n_iter;
  float errx, erry, errz, err2, err;
  int ntot = nmax-nmin;

  // Print to VMD
  if ( visu==1 ) { fprintf(fpos,"%i\n",ntot); fprintf(fpos,"title\n"); fprintf(fmom,"%i\n",ntot); fprintf(fmom,"title\n"); }
  for ( int i = nmin; i < nmax; i++ ) {
    if ( visu==1 ) { 
      if ( Zlist[i]==1 ) {
	fprintf(fpos,"P %f %f %f\n",posx[i],posy[i],posz[i]); fprintf(fmom,"P %f %f %f\n",momx[i],momy[i],momz[i]); 
      } else {
	fprintf(fpos,"N %f %f %f\n",posx[i],posy[i],posz[i]); fprintf(fmom,"N %f %f %f\n",momx[i],momy[i],momz[i]); 
      }
    }
  }

  // Initialize iteration array and print positions and momenta
  for ( int i = nmin; i < nmax; i++ ) {
    fakeposx[i] = posx[i]; fakeposy[i] = posy[i]; fakeposz[i] = posz[i];
  }

  // Iterate position until max iterations or acceptable error
  // Use most recent fakepos values
  n_iter = 0; err = 1;
  while ( (n_iter<max_iter) && (err > tol) ) {
    n_iter++;
    err2 = 0; err = 0;
    // For each particle, look at each other particle
#pragma omp parallel for private(j,dx,dy,dz,dr,dpx,dpy,dpz,dp,Fp)
    for ( int i = nmin; i < nmax; i++ ) {
      // Compute Inter-Particle Forces
      Fpx[i] = Fpy[i] = Fpz[i] = 0;
      for ( int j = nmin; j < nmax; j++ ) {	
	// No self-interactions
	if ( i == j ) continue;
	// Relative position and momentum
	dx = fakeposx[i] - fakeposx[j]; dy = fakeposy[i] - fakeposy[j]; dz = fakeposz[i] - fakeposz[j]; 
	dr = pow( dx*dx + dy*dy + dz*dz , 1.0/2.0 );
	dpx = 0.5*(momx[i] - momx[j]); dpy = 0.5*(momy[i] - momy[j]); dpz = 0.5*(momz[i] - momz[j]);
	dp = pow( dpx*dpx + dpy*dpy + dpz*dpz , 1.0/2.0 );
	// Momental Forces
	Fp = TotalFp(dr,dp,Slist[i],Slist[j]);
	// Force components
	Fpx[i] += Fp * dpx / dp; Fpy[i] += Fp * dpy / dp; Fpz[i] += Fp * dpz / dp;
      }// for PARTICLE j
    }// for PARTICLE i
    // Update particle position and compute error
    for ( int i = nmin; i < nmax; i++ ) {
      // A = 1/2 B0 (0,-z,y)
      Ay[i] = 0.5 * B0 * (-fakeposz[i]);
      Az[i] = 0.5 * B0 * (+fakeposy[i]);
      errx = fakeposx[i] - posx[i] - dt*(momx[i]/m + Fpx[i]);
      erry = fakeposy[i] - posy[i] - dt*(momy[i]/m - Zlist[i]*Ay[i]/m + Fpy[i]);
      errz = fakeposz[i] - posz[i] - dt*(momz[i]/m - Zlist[i]*Az[i]/m + Fpz[i]);
      err2 += errx*errx + erry*erry + errz*errz;
      fakeposx[i] = posx[i] + dt*(momx[i]/m + Fpx[i]);
      fakeposy[i] = posy[i] + dt*(momy[i]/m - Zlist[i]*Ay[i]/m + Fpy[i]);
      fakeposz[i] = posz[i] + dt*(momz[i]/m - Zlist[i]*Az[i]/m + Fpz[i]);
    }
    err = pow(err2,1.0/2.0);
  }// for IMPLICIT ITERATOR
  if ( err > tol ) { printf("Error tolerance exceeded! err = %.1e\n",err); }

  // Update actual position
  for ( int i = nmin; i < nmax; i++ ) {
    posx[i] = fakeposx[i]; posy[i] = fakeposy[i]; posz[i] = fakeposz[i];
  }

  // Get new momentum from new position
  // Uses most recent values in a race condition fashion, which is okay
  xsum=0; ysum=0; zsum=0; r2sum=0; pxsum=0; pysum=0; pzsum=0; p2sum=0; Etot=0;
  #pragma omp parallel for private(j,dx,dy,dz,dr,dpx,dpy,dpz,dp,Fp,F,Ei) reduction(+: xsum,ysum,zsum,r2sum,pxsum,pysum,pzsum,p2sum,Etot)
  for ( int i = nmin; i < nmax; i++ ) {
    // Update metrics
    Ei = ( momx[i]*momx[i] + momy[i]*momy[i] + momz[i]*momz[i] ) / (2.0*m);
    xsum += posx[i]; ysum += posy[i]; zsum += posz[i];  pxsum += momx[i]; pysum += momy[i]; pzsum += momz[i];
    r2sum += ( posx[i]*posx[i] + posy[i]*posy[i] + posz[i]*posz[i] );  p2sum += ( momx[i]*momx[i] + momy[i]*momy[i] + momz[i]*momz[i] );
    // Compute Inter-Particle Forces
    Fx[i] = Fy[i] = Fz[i] = 0; Fpx[i] = Fpy[i] = Fpz[i] = 0;
    for ( int j = nmin; j < nmax; j++ ) {	
      // No self-interactions
      if ( i == j ) continue;
      // Relative position and momentum
      dx = fakeposx[i] - fakeposx[j]; dy = fakeposy[i] - fakeposy[j]; dz = fakeposz[i] - fakeposz[j]; 
      dr = pow( dx*dx + dy*dy + dz*dz , 1.0/2.0 );
      dpx = 0.5*(momx[i] - momx[j]); dpy = 0.5*(momy[i] - momy[j]); dpz = 0.5*(momz[i] - momz[j]);
      dp = pow( dpx*dpx + dpy*dpy + dpz*dpz , 1.0/2.0 );
      // 
      Ei += 0.5 * TotalV(dr,dp,Zlist[i],Zlist[j],Slist[i],Slist[j]); // double counting
      F = TotalF(dr,dp,Zlist[i],Zlist[j],Slist[i],Slist[j]);
      Fp = TotalFp(dr,dp,Slist[i],Slist[j]);
      // Force components
      Fx[i] += F * dx / dr; Fy[i] += F * dy / dr; Fz[i] += F * dz / dr;
      Fpx[i] += Fp * dpx / dp; Fpy[i] += Fp * dpy / dp; Fpz[i] += Fp * dpz / dp;
    }// for PARTICLE j
    // Update particle momentum
    momx[i] = momx[i] + dt*(Fx[i] - mu*(momx[i]/m+Fpx[i]) - k*(posx[i]-CMx));
    momy[i] = momy[i] + dt*(Fy[i] + (0.5*Zlist[i]*B0/m)*(momz[i]-Zlist[i]*Az[i]) - mu*(momy[i]/m-Zlist[i]*Ay[i]/m+Fpy[i]) - k*(posy[i]-CMy));
    momz[i] = momz[i] + dt*(Fz[i] - (0.5*Zlist[i]*B0/m)*(momy[i]-Zlist[i]*Ay[i]) - mu*(momz[i]/m-Zlist[i]*Az[i]/m+Fpz[i]) - k*(posz[i]-CMz));
    // Ndd particle's energy to total
    Etot += Ei;
  }// for PARTICLE i

  // Output metrics
  ravg = pow( xsum*xsum + ysum*ysum + zsum*zsum , 1.0/2.0 ) / ntot;
  pavg = pow( pxsum*pxsum + pysum*pysum + pzsum*pzsum , 1.0/2.0 ) / ntot;
  p2avg = p2sum/ntot; r2avg = r2sum/ntot;
  if ( visu==1 ) { fprintf(fmetrics,"%f %f %f\n",Etot,pow(r2avg-ravg*ravg,1.0/2.0),(p2avg-pavg*pavg)/(2.0*m)); }

}
