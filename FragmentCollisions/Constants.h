#ifndef _CONSTANTS_H_
#define _CONSTANTS_H_


#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// Constants
extern const float hbar;
extern const float m;
extern const float e2;
extern const float e;

// Functional constants
float R_ConstantDensity(int A);
float V_ConstantDensity(int A);
float EF(int A);
float pF(int A);


#endif
