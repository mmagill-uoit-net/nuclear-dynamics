#include "Constants.h"



// Constants
// Constants
const float hbar = 197.3; // MeV-fm/c
const float m = 931.5; // Mev/c^2
const float e2 = 1.440; // MeV fm
const float e = 1.20; // MeV fm

// Functional constants
float R_ConstantDensity (int A) {
  return 1.2*pow(A,1.0/3.0); // fm
}

float V_ConstantDensity (int A) {
  float R = R_ConstantDensity(A);
  return (4./3.) * M_PI * R*R*R; // fm^3
}

// Approximately the Fermi energy
float EF (int A) {
  float V = V_ConstantDensity(A);
  return (hbar*hbar)/(2*m) * pow(3.0*M_PI*M_PI*A/V, 2.0/3.0); // MeV
}

// Approximately the Fermi momentum
float pF (int A) {
  float E = EF(A);
  return pow(2.0*m*E,1.0/2.0); // MeV/c
}
