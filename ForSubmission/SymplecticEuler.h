#ifndef _SYMPLECTICEULER_H_
#define _SYMPLECTICEULER_H_

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "Potentials.h"

void SymplecticEuler(float* posx, float* posy, float* posz,
		     float* fakeposx, float* fakeposy, float* fakeposz,
		     float* momx, float* momy, float* momz,
		     float* Fx, float* Fy, float* Fz,
		     float* Fpx, float* Fpy, float* Fpz,
		     int* Zlist, int* Slist,
		     float* Ax, float* Ay, float B0,
		     float CMx, float CMy, float CMz,
		     int nmin, int nmax, int A, int Z, int visu, float k, float mu,
		     float dt, int max_iter, float tol,
		     FILE* fpos, FILE* fmom, FILE* fmetrics);

#endif
