



for b in 000 050 060 070 080 090 100 110 120
do

    cat data/count_${b}_0e+00.xyz | awk '{print $1/11.}' | paste -sd+ | bc >> TotalFree_NoB.dat
    cat data/count_${b}_0e+00.xyz | awk '{print $2/11.}' | paste -sd+ | bc >> NeutFree_NoB.dat

    cat data/count_${b}_1e+02.xyz | awk '{print $1/11.}' | paste -sd+ | bc >> TotalFree_WiB.dat
    cat data/count_${b}_1e+02.xyz | awk '{print $2/11.}' | paste -sd+ | bc >> NeutFree_WiB.dat

done
