#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>

#include "Constants.h"
#include "Potentials.h"
#include "ComputeGroundState.h"
#include "SymplecticEuler.h"

// // Note on units // //
// mass in MeV/c^2
// energy in MeV
// momentum in MeV/c
// distance in fm
// time in fm/c

// // Main
int main(int argc, char* argv[]) {

  // // 

  // Parse arguments
  if ( argc != 4 ) { printf("%s [yoff] [B0] [seed]\n",argv[0]); return 1; }
  float yoff = strtof(argv[1],NULL); // fm
  float B0 = strtof(argv[2],NULL);
  int rseed=strtol(argv[3],NULL,10); srand(rseed);

  // Simulation Parameters
  int visu = 1; // Whether or not to visualize equilibration 
  int xoff = 20; // fm
  float mu = 115; // MeV/fm
  float k = 15; // Keeps nuclei bound during equilibration
  int max_iter = 50; // Max iterations per timestep
  float tol = 1e-3; // Error tolerance on position

  // Times
  float T_eql1 = 50; float T_eql2 = 100; float T_eql3 = 100; // fm/c
  float T_sim = 200; // fm/c
  float dt = 0.1; // fm/c
  float Nt_eql1=round(T_eql1/dt); float Nt_eql2=round(T_eql2/dt); float Nt_eql3=round(T_eql3/dt); float Nt_sim = round(T_sim/dt);

  // //

  // Randomly decide fragments: U-235 fission
  // Mass spectrum Gaussian centered at A=90 or A=130, sigma=5
  int A1, A2;
  /*
  float rndA = (float)rand()/RAND_MAX; float rndB = (float)rand()/RAND_MAX;
  float GrndA = cos(2*M_PI*rndB) * pow(-2*log(rndA),0.5);
  float GrndB = sin(2*M_PI*rndB) * pow(-2*log(rndA),0.5);
  float PeakFlipA = (float)rand()/RAND_MAX; float PeakFlipB = (float)rand()/RAND_MAX;
  if ( PeakFlipA > 0.5 ) { A1 = round(GrndA*5 + 90); } else { A1 = round(GrndA*5 + 130); }
  if ( PeakFlipB > 0.5 ) { A2 = round(GrndB*5 + 90); } else { A2 = round(GrndB*5 + 130); }
  */
  A1 = 90; A2 = 130; // Don't have time to sample properly

  // Compute resultant parameters.
  int Z1 = A1/2; int Z2 = A2/2; // Reasonable estimate
  /*
  // Each fission releases ~170 MeV. Momentum approximated by mv here.
  float p1 = A1*m * pow( 170. / (m*A1*(1 + (A1/(235-A1)))) , 0.5 );
  float p2 = A2*m * pow( 170. / (m*A2*(1 + (A2/(235-A2)))) , 0.5 );

  // Randomly assign direction
  float rnd1 = (float)rand()/RAND_MAX; float rnd2 = (float)rand()/RAND_MAX;
  float p1x = p1*cos(2*M_PI*rnd1)*sin(acos(1-2*rnd2));
  float p1y = p1*sin(2*M_PI*rnd1)*sin(acos(1-2*rnd2));
  float p1z = p1*cos(acos(1-2*rnd2));
  rnd1 = (float)rand()/RAND_MAX; rnd2 = (float)rand()/RAND_MAX;
  float p2x = p2*cos(2*M_PI*rnd1)*sin(acos(1-2*rnd2));
  float p2y = p2*sin(2*M_PI*rnd1)*sin(acos(1-2*rnd2));
  float p2z = p2*cos(acos(1-2*rnd2));
  */

  // For cross-section analysis, can just do linear collisions.
  float Erel = 200; // MeV per Nucleon
  float p1 = 0;
  float p1x = p1;
  float p1y = 0;
  float p1z = 0;
  float p2 = pow(2*m*Erel,1.0/2.0);
  float p2x = p2;
  float p2y = 0;
  float p2z = 0;

  // Allocations
  float* posx = malloc(sizeof(float)*(A1+A2)); float* posy = malloc(sizeof(float)*(A1+A2)); float* posz = malloc(sizeof(float)*(A1+A2));
  float* momx = malloc(sizeof(float)*(A1+A2)); float* momy = malloc(sizeof(float)*(A1+A2)); float* momz = malloc(sizeof(float)*(A1+A2));
  float* fakeposx = malloc(sizeof(float)*(A1+A2)); float* fakeposy = malloc(sizeof(float)*(A1+A2)); float* fakeposz = malloc(sizeof(float)*(A1+A2));
  float* Fpx = malloc(sizeof(float)*(A1+A2)); float* Fpy = malloc(sizeof(float)*(A1+A2)); float* Fpz = malloc(sizeof(float)*(A1+A2));
  float* Fx = malloc(sizeof(float)*(A1+A2)); float* Fy = malloc(sizeof(float)*(A1+A2)); float* Fz = malloc(sizeof(float)*(A1+A2));
  int* Zlist = malloc(sizeof(int)*(A1+A2)); int* Slist = malloc(sizeof(int)*(A1+A2));
  float* Ay = malloc(sizeof(float)*(A1+A2)); float* Az = malloc(sizeof(float)*(A1+A2)); 

  // Specify protons
  for ( int i = 0; i < Z1; i++ ) { Zlist[i] = 1; } for ( int i = Z1; i < A1; i++ ) { Zlist[i] = 0; }
  for ( int i = A1; i < A1+Z2; i++ ) { Zlist[i] = 1; } for ( int i = A1+Z2; i < A1+A2; i++ ) { Zlist[i] = 0; }

  // Specify spins
  for ( int i = 0; i < A1/4; i++ ) { Slist[i] = 1; } for ( int i = A1/4; i < A1/2; i++ ) { Slist[i] = 2; }
  for ( int i = A1/2; i < 3*A1/4; i++ ) { Slist[i] = 3; } for ( int i = 3*A1/4; i < A1; i++ ) { Slist[i] = 4; }
  for ( int i = A1; i < A1+A2/4; i++ ) { Slist[i] = 1; } for ( int i = A1+A2/4; i < A1+A2/2; i++ ) { Slist[i] = 2; }
  for ( int i = A1+A2/2; i < A1+3*A2/4; i++ ) { Slist[i] = 3; } for ( int i = A1+3*A2/4; i < A1+A2; i++ ) { Slist[i] = 4; }
 
  // Compute or load first ground state
  char fgroundsave_name[256]; char fposground_name[256]; char fmomground_name[256]; char fmetricsground_name[256];
  sprintf(fgroundsave_name,"ground-states/center_%03d_%03d_%03d_%.0e_%04d.dat",(int)(10*yoff),A1,Z1,B0,rseed);
  sprintf(fposground_name,"data/posground_center_%03d_%03d_%03d_%.0e_%04d.xyz",(int)(10*yoff),A1,Z1,B0,rseed);
  sprintf(fmomground_name,"data/momground_center_%03d_%03d_%03d_%.0e_%04d.xyz",(int)(10*yoff),A1,Z1,B0,rseed);
  sprintf(fmetricsground_name,"data/metricsground_center_%03d_%03d_%03d_%.0e_%04d.xyz",(int)(10*yoff),A1,Z1,B0,rseed);
  int GroundExists = access(fgroundsave_name,R_OK);
  if ( GroundExists == -1 ) {
    printf("Computing First Ground State\n");
    FILE* fgroundsave = fopen(fgroundsave_name,"w"); FILE* fposground = fopen(fposground_name,"w");
    FILE* fmomground = fopen(fmomground_name,"w"); FILE* fmetricsground = fopen(fmetricsground_name,"w");
    ComputeGroundState(posx,posy,posz,fakeposx,fakeposy,fakeposz,momx,momy,momz,Fx,Fy,Fz,Fpx,Fpy,Fpz,Zlist,Slist,
		       Ay,Az,B0, 0,0,0, 0,A1,A1,Z1, visu,k,mu, dt,Nt_eql1,Nt_eql2,Nt_eql3, max_iter,tol, 
		       fposground,fmomground,fmetricsground);
    SaveGroundState(posx,posy,posz,momx,momy,momz,0,A1,fgroundsave);
    fclose(fmetricsground); fclose(fmomground); fclose(fposground); fclose(fgroundsave);
    printf("\r%02d%% Complete   \n",100); fflush(stdout);
  } else {
    printf("Loading Ground State\n");
    FILE* fgroundsave = fopen(fgroundsave_name,"r"); 
    LoadGroundState(posx,posy,posz,momx,momy,momz,0,A1,fgroundsave);
    fclose(fgroundsave);
    printf("Done.\n");
  }

  // Initialize and equilibrate second nucleus
  sprintf(fgroundsave_name,"ground-states/projectile_%03d_%03d_%03d_%.0e_%04d.dat",(int)(10*yoff),A2,Z2,B0,rseed);
  sprintf(fposground_name,"data/posground_projectile_%03d_%03d_%03d_%.0e_%04d.xyz",(int)(10*yoff),A2,Z2,B0,rseed);
  sprintf(fmomground_name,"data/momground_projectile_%03d_%03d_%03d_%.0e_%04d.xyz",(int)(10*yoff),A2,Z2,B0,rseed);
  sprintf(fmetricsground_name,"data/metricsground_projectile_%03d_%03d_%03d_%.0e_%04d.xyz",(int)(10*yoff),A2,Z2,B0,rseed);
  GroundExists = access(fgroundsave_name,R_OK);
  if ( GroundExists == -1 ) {
    printf("Computing Second Ground State\n");
    FILE* fgroundsave = fopen(fgroundsave_name,"w"); FILE* fposground = fopen(fposground_name,"w");
    FILE* fmomground = fopen(fmomground_name,"w"); FILE* fmetricsground = fopen(fmetricsground_name,"w");
    ComputeGroundState(posx,posy,posz,fakeposx,fakeposy,fakeposz,momx,momy,momz,Fx,Fy,Fz,Fpx,Fpy,Fpz,Zlist,Slist,
		       Ay,Az,B0, -xoff,yoff,0, A1,(A1+A2),A2,Z2, visu,k,mu, dt,Nt_eql1,Nt_eql2,Nt_eql3, max_iter,tol, 
		       fposground,fmomground,fmetricsground);
    SaveGroundState(posx,posy,posz,momx,momy,momz,A1,A1+A2,fgroundsave);
    fclose(fmetricsground); fclose(fmomground); fclose(fposground); fclose(fgroundsave);
    printf("\r%02d%% Complete   \n",100); fflush(stdout);
  } else {
    printf("Loading Ground State\n");
    FILE* fgroundsave = fopen(fgroundsave_name,"r"); 
    LoadGroundState(posx,posy,posz,momx,momy,momz,A1,A1+A2,fgroundsave);
    fclose(fgroundsave);
    printf("Done.\n");
  }

  // // 

  // Assign boosts
  for ( int i = 0; i < A1; i++ ) { momx[i] = p1x+momx[i]; momy[i] = p1y+momy[i]; momz[i] = p1z+momz[i]; }
  for ( int i = A1; i < (A1+A2); i++ ) { momx[i] = p2x+momx[i]; momy[i] = p2y+momy[i]; momz[i] = p2z+momz[i]; }

  // Remove viscosity, start outputting
  k=0; mu=0;

  // Collision output files
  char fpos_name[256]; char fmom_name[256]; char fmetrics_name[256];
  sprintf(fpos_name,"data/pos_%03d_%03d_%03d_%03d_%03d_%.0e_%04d.xyz",(int)(10*yoff),A1,Z1,A2,Z2,B0,rseed);
  sprintf(fmom_name,"data/mom_%03d_%03d_%03d_%03d_%03d_%.0e_%04d.xyz",(int)(10*yoff),A1,Z1,A2,Z2,B0,rseed);
  sprintf(fmetrics_name,"data/metrics_%03d_%03d_%03d_%03d_%03d_%.0e_%04d.xyz",(int)(10*yoff),A1,Z1,A2,Z2,B0,rseed);
  FILE* fpos = fopen(fpos_name,"w"); FILE* fmom = fopen(fmom_name,"w"); FILE* fmetrics = fopen(fmetrics_name,"w");

  // Main Loop
  printf("Calculating Collision\n");
  for ( int nt = 0; nt < Nt_sim; nt++ ) {

    // Update progress
    if ( (nt % ((int)Nt_sim/100))==0 ){ printf("\r%02d%% Complete...",(int)((100*nt)/Nt_sim)); fflush(stdout); }

    SymplecticEuler(posx,posy,posz,fakeposx,fakeposy,fakeposz,momx,momy,momz,Fx,Fy,Fz,Fpx,Fpy,Fpz,Zlist,Slist,
		    Ay,Az,B0, 0,0,0, 0,(A1+A2),A1,Z1, visu,k,mu, dt,max_iter,tol, fpos,fmom,fmetrics);

  }// for TIME

  // // 

  // Count free nucleons: far from all other nucleons by end of collision
  char fcount_name[256];
  sprintf(fcount_name,"data/count_%03d_%.0e.dat",(int)(10*yoff),B0);
  FILE* fcount = fopen(fcount_name,"a");
  float dx,dy,dz,dr; int nclose; float dist_thresh = 3.5;
  int nfree, nfree_neutrons;
  nfree=0; nfree_neutrons=0;
  for ( int i = 0; i < A1+A2; i++ ) {
    nclose = 0;
    for ( int j = 0; j < A1+A2; j++ ) {	
      // No self-interactions
      if ( i == j ) continue;
      // Distance
      dx = fakeposx[i] - fakeposx[j]; dy = fakeposy[i] - fakeposy[j]; dz = fakeposz[i] - fakeposz[j]; 
      dr = pow( dx*dx + dy*dy + dz*dz , 1.0/2.0 );
      // Close?
      if ( dr < dist_thresh) { nclose++; }
    }
    // If particle i has fewer than 3 neighbours
    if ( nclose < 3 ) { 
      nfree++;
      if ( Zlist[i] == 0 ) { nfree_neutrons++; }
    }
  }
  fprintf(fcount,"%d %d\n",nfree,nfree_neutrons);
  fclose(fcount);


  // //

  // Finish
  printf("\r%02d%% Complete   \n",100); fflush(stdout);
  free(posx); free(posy); free(posz);
  free(momx); free(momy); free(momz);
  free(Zlist);
  fclose(fmetrics); fclose(fmom); fclose(fpos);
  return 0;
}


