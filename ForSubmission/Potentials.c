#include "Potentials.h"

// From Wilets II
const float VSR = 690.50; // MeV-fm
const float VA = 232.16; // MeV-fm
const float VLR = 22.37; // MeV-fm
const float KSR = 1.28; // fm^-1
const float KA = 0.64; // fm^-1
const float KLR = 0.32; // MeV-fm
const float xi = 2.14;

// // Function definitions
float VN(float dr) {
  // V(r) = [ VSR*exp(-KSR*r) - VA*exp(-KA*r) + VLR*exp(-KLR*r) ] / r
  return ( (VSR*exp(-KSR*dr)-VA*exp(-KA*dr)+VLR*exp(-KLR*dr)) / dr );
}

float FN(float dr) {
  // F(r) = [ (1+r*KSR)*VSR*exp(-KSR*r) - (1+r*KA)*VA*exp(-KA*r) + (1+r*KLR)*VLR*exp(-KA*r) ] / r^2
  return ( ( (1+dr*KSR)*VSR*exp(-KSR*dr) - (1+dr*KA)*VA*exp(-KA*dr) + (1+dr*KLR)*VLR*exp(-KLR*dr) ) / (dr*dr) );
}

float VC(float dr, int Zi, int Zj) {
  // V(r) = ZiZj e2 / r
  return ( Zi*Zj * e2 / dr );
}

float FC(float dr, int Zi, int Zj) {
  // F(r) = ZiZj e2 / r^2
  return ( Zi*Zj * e2 / (dr*dr) );
}

float VP(float dr, float dp, int Si, int Sj) {
  // VP(r,p) = (xi^2 hbar^2 / 5 m r^2) * exp(-2.5*[(p r/xi hbar)^4-1]) * delta(Si, Sj)
  return ( (xi*xi*hbar*hbar/(5.0*m*dr*dr)) * exp(-2.5*( pow(dp*dr/(xi*hbar),4.0) - 1 )) ) * (Si == Sj);
}

float FP(float dr, float dp, int Si, int Sj) {
  // dVP/dr = - (xi^2 hbar^2 / 5 m r^2) * exp(-2.5*[(p r/xi hbar)^4-1]) * (2/r^3) * ( 1 + 5 (p r/xi hbar)^4 )
  return ( (xi*xi*hbar*hbar/(5.0*m))*exp(-2.5*( pow(dp*dr/(xi*hbar),4.0)-1))*(2.0/(dr*dr*dr))*(1 + 5*pow(dp*dr/(xi*hbar),4.0)) ) * (Si == Sj);
}

float FpP(float dr, float dp, int Si, int Sj) {
  // dVP/dp = - (p^3/m) * pow(dr/(xi*hbar),2.0) * exp(-2.5*( pow(dp*dr/(xi*hbar),4.0) - 1 )) ) * delta(Si,Sj)
  return ( - ((dp*dp*dp)/m) * pow(dr/(xi*hbar),2.0) * exp(-2.5*( pow(dp*dr/(xi*hbar),4.0) - 1 )) ) * (Si == Sj);
}


// //

float TotalV(float dr, float dp, int Zi, int Zj, int Si, int Sj) {
  return VN(dr) + VC(dr,Zi,Zj) + VP(dr,dp,Si,Sj);
}

float TotalF(float dr, float dp, int Zi, int Zj, int Si, int Sj) {
  return FN(dr) + FC(dr,Zi,Zj) + FP(dr,dp,Si,Sj);
}

float TotalFp(float dr, float dp, int Si, int Sj) {
  return FpP(dr,dp,Si,Sj);
}
