To compile, simply use

   make

To run the code, use the syntax

   ./NbodyNucleus.exe [yoff] [B0] [seed]

where 

   yoff is the impact parameter
   B0 is the magnetic field strength in arbitrary units
   seed is the random seed

The simulations in the report used B0=100, as per the cases in run_cases.sh

---

After running a given (yoff,B0,seed) value, the code will save the
 ground states (i.e. the post-equilibration nuclear configurations)
 so that the case can be re-run quickly in the future. These are saved
 in the ground-states folder.

By default, the code uses OpenMP and tries to run on as many threads
 as are available. To disable this behaviour, remove the corresponding
 line from the Makefile and recompile.

---

While running, the code will send messages to the terminal if the
 error in the iterative loop does not converge sufficiently 
 within the maximum number of iterations. This usually occurs
 for the first few stages of equilibration, and this is of
 no concern. If errors are repeatedly exceeded during the actual
 collision calculation, the max_iter and tol variables may need to
 be tuned in NbodyNucleus.c.

The collision data is saved in the data folder. Each collision
 produces 9 or 10 output files, described below.
 
Number of free nucleons and free neutrons are concatenated to 
 the end of

  count_[10*yoff]_[B0].dat

Three metrics of interest are tracked during the collision in

  metrics_[10*yoff]_[A1]_[Z1]_[A2]_[Z2]_[B0]_[rseed].xyz

The first column is total energy, the second is RMS radius, and
 the third is average kinetic energy.

The positions of the nuclei are tracked in 

  pos_[10*yoff]_[A1]_[Z1]_[A2]_[Z2]_[B0]_[rseed].xyz

and their momenta are tracked in

  mom_[10*yoff]_[A1]_[Z1]_[A2]_[Z2]_[B0]_[rseed].xyz

Both these files can be viewed directly in vmd.

The ~ground files describe the same data as above, but during
 the equilibration of the center (target) nucleus and projectile
 nucleus, as given by the filenames.
