
#include "ComputeGroundState.h"



// Starting with random nucleon arrangement, find ground state
// I chose a three-stage equilibration
// After first equilibration, remove binding force
// After second equilibration, reduce viscosity

// ntot is the number of particles to print to VMD
// A is the number of particles that are actually evolved

void ComputeGroundState(float* posx, float* posy, float* posz,
			float* fakeposx, float* fakeposy, float* fakeposz,
			float* momx, float* momy, float* momz,
			float* Fx, float* Fy, float* Fz,
			float* Fpx, float* Fpy, float* Fpz,
			int* Zlist, int* Slist,
			float* Ay, float* Az, float B0,
			float CMx, float CMy, float CMz,
			int nmin, int nmax, int A, int Z, int visu, float k, float mu,
			float dt, int Nt_eql1, int Nt_eql2, int Nt_eql3,
			int max_iter, float tol,
			FILE* fpos, FILE* fmom, FILE* fmetrics) {

  // Declarations
  int j; // For OpenMP
  float rnd1, rnd2, rnd3; float r, phi, theta;
  float dr, dx, dy, dz; float dp, dpx, dpy, dpz;
  float F, Fp;
  float Ei, Etot;
  float xsum, ysum, zsum; float ravg, r2sum, r2avg;
  float pxsum, pysum, pzsum; float pavg, p2sum, p2avg;
  int n_iter;
  float errx, erry, errz, err2, err;


  // Initial positions uniform in a sphere of radius R
  // Initial momenta are chosen at (3/5) of Fermi momentum
  float R = R_ConstantDensity(A);
  float p = 0.1*pF(A);
  for ( int i = nmin; i < nmax; i++ ) {
    // Positions
    rnd1 = (float)rand()/RAND_MAX; rnd2 = (float)rand()/RAND_MAX; rnd3 = (float)rand()/RAND_MAX;
    r = R*pow(rnd1,1.0/3.0); phi = 2*M_PI*rnd2; theta = acos(1-2*rnd3);
    posx[i] = CMx+r*cos(phi)*sin(theta); posy[i] = CMy+r*sin(phi)*sin(theta); posz[i] = CMz+r*cos(theta);

    // Momenta
    rnd1 = (float)rand()/RAND_MAX; rnd2 = (float)rand()/RAND_MAX; rnd3 = (float)rand()/RAND_MAX;
    r = p*pow(rnd1,1.0/3.0); phi = 2*M_PI*rnd2; theta = acos(1-2*rnd3);
    momx[i] = r*cos(phi)*sin(theta); momy[i] = r*sin(phi)*sin(theta); momz[i] = r*cos(theta);
  }

  // Main Loop
  int Nt = Nt_eql1 + Nt_eql2 + Nt_eql3;
  for ( int nt = 0; nt < Nt; nt++ ) {

    // Update progress
    if ( (nt % ((int)Nt/100))==0 ){ printf("\r%02d%% Complete...",(int)((100*nt)/Nt)); fflush(stdout); }

    // Equilibration stages
    if ( nt == Nt_eql1) { k=0; }
    if ( nt == Nt_eql1+Nt_eql2) { mu=mu/10.; }

    // Integrate
    SymplecticEuler(posx,posy,posz,
		    fakeposx,fakeposy,fakeposz,
		    momx,momy,momz,
		    Fx,Fy,Fz,
		    Fpx,Fpy,Fpz,
		    Zlist,Slist,
		    Ay,Az,B0, CMx,CMy,CMz,
		    nmin,nmax, A, Z, visu,k,mu,
		    dt, max_iter, tol,
		    fpos,fmom,fmetrics);

  }// for TIME

}


void SaveGroundState(float* posx, float* posy, float* posz,
		     float* momx, float* momy, float* momz,
		     int Amin, int Amax, FILE* fsave) {
  for ( int i = Amin; i < Amax; i++ ) {
    fprintf(fsave,"%f %f %f %f %f %f\n",posx[i],posy[i],posz[i],momx[i],momy[i],momz[i]);
  }
}


void LoadGroundState(float* posx, float* posy, float* posz,
		     float* momx, float* momy, float* momz,
		     int Amin, int Amax, FILE* fsave) {

  for ( int i = Amin; i < Amax; i++ ) {
    fscanf(fsave,"%f %f %f %f %f %f\n",&posx[i],&posy[i],&posz[i],&momx[i],&momy[i],&momz[i]);
  }
}
