#ifndef _COMPUTEGROUNDSTATE_H_
#define _COMPUTEGROUNDSTATE_H_

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "Constants.h"
#include "SymplecticEuler.h"

void ComputeGroundState(float* posx, float* posy, float* posz,
			float* fakeposx, float* fakeposy, float* fakeposz,
			float* momx, float* momy, float* momz,
			float* Fx, float* Fy, float* Fz,
			float* Fpx, float* Fpy, float* Fpz,
			int* Zlist, int* Slist,
			float* Ay, float* Az, float B0,
			float CMx, float CMy, float CMz,
			int nmin, int nmax, int A, int Z, int visu, float k, float mu,
			float dt, int Nt_eql1, int Nt_eql2, int Nt_eql3,
			int max_iter, float tol,
			FILE* fpos, FILE* fmom, FILE* fmetrics);

void SaveGroundState(float* posx, float* posy, float* posz,
		     float* momx, float* momy, float* momz,
		     int Amin, int Amax, FILE* fsave);

void LoadGroundState(float* posx, float* posy, float* posz,
		     float* momx, float* momy, float* momz,
		     int Amin, int Amax, FILE* fsave);
			


#endif
