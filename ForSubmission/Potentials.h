#ifndef _POTENTIALS_H_
#define _POTENTIALS_H_

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "Constants.h"

// Parameters for nuclear force
extern const float VR;
extern const float VA;
extern const float VLR;
extern const float KR;
extern const float KA;
extern const float KLR;

// Parameters for Pauli core
extern const float xi;

// Function declarations
float VN(float dr);
float FN(float dr);
float VC(float dr, int Zi, int Zj);
float FC(float dr, int Zi, int Zj);
float VP(float dr, float dp, int Si, int Sj);
float FP(float dr, float dp, int Si, int Sj);
float FpP(float dr, float dp, int Si, int Sj);
//float VM(float x, float y, float 
float FpM(float x, float y, float px, float py);

float TotalV(float dr, float dp, int Zi, int Zj, int Si, int Sj);
float TotalF(float dr, float dp, int Zi, int Zj, int Si, int Sj);
float TotalFp(float dr, float dp, int Si, int Sj);











#endif
