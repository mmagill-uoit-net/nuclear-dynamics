#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>

#include "Potentials.h"
#include "ComputeGroundState.h"
#include "SymplecticEuler.h"

// // Note on units // //
// mass in MeV/c^2
// energy in MeV
// momentum in MeV/c
// distance in fm
// time in fm/c

// // Main
int main(int argc, char* argv[]) {
  // Parse arguments
  if ( argc != 5 ) { printf("%s [A] [Z] [B0] [seed]\n",argv[0]); return 1; }
  int A = strtol(argv[1],NULL,10);
  int Z = strtol(argv[2],NULL,10);
  float B0 = strtof(argv[3],NULL);
  int rseed=strtol(argv[4],NULL,10); srand(rseed);

  // Simulation Parameters
  int visu = 1; // Whether or not to visualize equilibration 
  int ntot = 2*A; // For two nuclei
  float ERel = 100.0; float pRel = pow(2.0*m*ERel,1.0/2.0); // Collision energy/momentum
  float xoff = 40; // Offset in Radii
  float yoff = 0.; // Offset in Radii
  float mu = 115; // MeV/fm
  float k = 15; // Keeps nuclei bound during equilibration
  int max_iter = 50; // Max iterations per timestep
  float tol = 1e-4; // Error tolerance on position

  // Times
  float T_eql1 = 300; float T_eql2 = 200; float T_eql3 = 100; // fm/c
  float T_sim = 0; // fm/c
  float dt = 0.1; // fm/c
  float Nt_eql1=round(T_eql1/dt); float Nt_eql2=round(T_eql2/dt); float Nt_eql3=round(T_eql3/dt); float Nt_sim = round(T_sim/dt);

  // Allocations
  float* posx = malloc(sizeof(float)*2*A); float* posy = malloc(sizeof(float)*2*A); float* posz = malloc(sizeof(float)*2*A);
  float* momx = malloc(sizeof(float)*2*A); float* momy = malloc(sizeof(float)*2*A); float* momz = malloc(sizeof(float)*2*A);
  float* fakeposx = malloc(sizeof(float)*2*A); float* fakeposy = malloc(sizeof(float)*2*A); float* fakeposz = malloc(sizeof(float)*2*A);
  float* Fpx = malloc(sizeof(float)*2*A); float* Fpy = malloc(sizeof(float)*2*A); float* Fpz = malloc(sizeof(float)*2*A);
  float* Fx = malloc(sizeof(float)*2*A); float* Fy = malloc(sizeof(float)*2*A); float* Fz = malloc(sizeof(float)*2*A);
  int* Zlist = malloc(sizeof(int)*2*A); int* Slist = malloc(sizeof(int)*2*A);
  float* Ay = malloc(sizeof(float)*2*A); float* Az = malloc(sizeof(float)*2*A); 

  // Specify protons
  for ( int i = 0; i < Z; i++ ) { Zlist[i] = 1; } for ( int i = Z; i < A; i++ ) { Zlist[i] = 0; }
  for ( int i = A; i < A+Z; i++ ) { Zlist[i] = 1; } for ( int i = A+Z; i < 2*A; i++ ) { Zlist[i] = 0; }

  // Specify spins
  for ( int i = 0; i < A/4; i++ ) { Slist[i] = 1; } for ( int i = A/4; i < A/2; i++ ) { Slist[i] = 2; }
  for ( int i = A/2; i < 3*A/4; i++ ) { Slist[i] = 3; } for ( int i = 3*A/4; i < A; i++ ) { Slist[i] = 4; }
  for ( int i = A; i < A+A/4; i++ ) { Slist[i] = 1; } for ( int i = A+A/4; i < A+A/2; i++ ) { Slist[i] = 2; }
  for ( int i = A+A/2; i < A+3*A/4; i++ ) { Slist[i] = 3; } for ( int i = A+3*A/4; i < 2*A; i++ ) { Slist[i] = 4; }
 
  // Compute or load ground state
  char fgroundsave_name[256]; char fposground_name[256]; char fmomground_name[256]; char fmetricsground_name[256];
  sprintf(fgroundsave_name,"ground-states/%03d_%03d_%.0e_%04d.dat",A,Z,B0,rseed);
  sprintf(fposground_name,"data/posground_%03d_%03d_%.0e_%04d.xyz",A,Z,B0,rseed);
  sprintf(fmomground_name,"data/momground_%03d_%03d_%.0e_%04d.xyz",A,Z,B0,rseed);
  sprintf(fmetricsground_name,"data/metricsground_%03d_%03d_%.0e_%04d.xyz",A,Z,B0,rseed);
  int GroundExists = access(fgroundsave_name,R_OK);
  if ( GroundExists == -1 ) {
    printf("Computing Ground State\n");
    FILE* fgroundsave = fopen(fgroundsave_name,"w"); FILE* fposground = fopen(fposground_name,"w");
    FILE* fmomground = fopen(fmomground_name,"w"); FILE* fmetricsground = fopen(fmetricsground_name,"w");
    ComputeGroundState(posx,posy,posz,fakeposx,fakeposy,fakeposz,momx,momy,momz,Fx,Fy,Fz,Fpx,Fpy,Fpz,Zlist,Slist,
		       Ay,Az,B0, 0,0,0, 0,A,A,Z, visu,k,mu, dt,Nt_eql1,Nt_eql2,Nt_eql3, max_iter,tol, 
		       fposground,fmomground,fmetricsground);
    SaveGroundState(posx,posy,posz,momx,momy,momz,A,fgroundsave);
    fclose(fmetricsground); fclose(fmomground); fclose(fposground); fclose(fgroundsave);
    printf("\r%02d%% Complete   \n",100); fflush(stdout);
  } else {
    printf("Loading Ground State\n");
    FILE* fgroundsave = fopen(fgroundsave_name,"r"); 
    LoadGroundState(posx,posy,posz,momx,momy,momz,A,fgroundsave);
    fclose(fgroundsave);
    printf("Done.\n");
  }


  // Set up for collision
  char fpos_name[256]; char fmom_name[256]; char fmetrics_name[256];
  sprintf(fpos_name,"data/pos_%03d_%03d_%.0e_%04d.xyz",A,Z,B0,rseed);
  sprintf(fmom_name,"data/mom_%03d_%03d_%.0e_%04d.xyz",A,Z,B0,rseed);
  sprintf(fmetrics_name,"data/metrics_%03d_%03d_%.0e_%04d.xyz",A,Z,B0,rseed);
  FILE* fpos = fopen(fpos_name,"w"); FILE* fmom = fopen(fmom_name,"w"); FILE* fmetrics = fopen(fmetrics_name,"w");

  // Initialize and equilibrate second nucleus
  FILE* fpostest = fopen("data/postest.xyz","w");
  FILE* fmomtest = fopen("data/momtest.xyz","w");
  FILE* fmettest = fopen("data/mettest.xyz","w");
  float R = R_ConstantDensity(A);
  printf("Equilibrating Second Nucleus\n");
  /*  ComputeGroundState(posx,posy,posz,fakeposx,fakeposy,fakeposz,momx,momy,momz,Fx,Fy,Fz,Fpx,Fpy,Fpz,Zlist,Slist,
		     Ay,Az,B0, -xoff*R,yoff*R,0, A,2*A,A,Z, 1,k,mu, dt,Nt_eql1,Nt_eql2,Nt_eql3, max_iter,tol, 
		     fpostest,fmomtest,fmettest);*/
  printf("\r%02d%% Complete   \n",100); fflush(stdout);
  for ( int i = A; i < ntot; i++ ) { momx[i] = pRel+momx[i]; momy[i] = momy[i]; momz[i] = momz[i]; }

  // Remove viscosity, start outputting
  k=0; mu=0; visu=1; 

  // Main Loop
  ntot = 2*A;
  printf("Calculating Collision\n");
  for ( int nt = 0; nt < Nt_sim; nt++ ) {

    // Update progress
    if ( (nt % ((int)Nt_sim/100))==0 ){ printf("\r%02d%% Complete...",(int)((100*nt)/Nt_sim)); fflush(stdout); }

    SymplecticEuler(posx,posy,posz,fakeposx,fakeposy,fakeposz,momx,momy,momz,Fx,Fy,Fz,Fpx,Fpy,Fpz,Zlist,Slist,
		    Ay,Az,B0, 0,0,0, 0,ntot,A,Z, visu,k,mu, dt,max_iter,tol, fpos,fmom,fmetrics);

  }// for TIME

  // Finish
  printf("\r%02d%% Complete   \n",100); fflush(stdout);
  free(posx); free(posy); free(posz);
  free(momx); free(momy); free(momz);
  free(Zlist);
  fclose(fmetrics); fclose(fmom); fclose(fpos);
  return 0;
}


